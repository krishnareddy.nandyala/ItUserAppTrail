import { Component, ViewChild,OnInit } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../providers/service/service';
//import { ScreenOrientation } from '@ionic-native/screen-orientation';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  userdata:{};
  userDetails: {};
  @ViewChild(Nav) nav: Nav;

  rootPage: any = "UserPage";

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public platform: Platform, public statusBar: StatusBar,
    private serviceProvider: ServiceProvider,
    public splashScreen: SplashScreen) {
    this.initializeApp();
    this.test();
  
    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'user', component: "UserPage" ,icon: 'home'},
      { title: 'Dash Board', component: "DashboardPage" ,icon: 'logo-buffer'},
      { title: 'Profile', component: "ProfilePage" ,icon: 'people'},
      { title: 'Log Out', component: "UserPage" ,icon: 'log-out'},
     
    ];
   
   
  }
  
  ngOnInit() {

    this.userDetails = this.serviceProvider.userDetails;
    
   
    console.log("sowjanya:",this.userDetails);
  }

  test(){
    this.userDetails = {
      name:localStorage.getItem('name'),
      email:localStorage.getItem('email'),
      PhoneNumber:localStorage.getItem('PhoneNumber'),
      location:localStorage.getItem('location'),
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

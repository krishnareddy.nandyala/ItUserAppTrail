import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, LoadingController, NavParams, ActionSheetController, AlertController, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {


  aadharfront: boolean = false;
  aadharbackside: boolean = false;

  images: boolean = false;
  display: any;
  transscreen: boolean = false;
  IdproofsList1 = [];
  aadharProofList1 = [];
  Aadhar: boolean = false;
  IdProofs: boolean = false;
  idproofs: any;
  aadharproofs: any;
  myphoto: any;
  constructor(public navCtrl: NavController,
    private serviceProvider: ServiceProvider,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public alert: AlertController,
    public navParams: NavParams, public storage: Storage, public actionsheet: ActionSheetController) {
    this.UserReg = this.navParams.get('register');
    // this.UserLogin = this.navParams.get()
    this.aadharproofs = this.navParams.get('aadharProofImages');

    this.idproofs = this.navParams.get('idProofImages');
    //  this.register.IDProofUrl.imageFront = this.idproofs.IDProofUrl.imageFront;
    //  this.register.IDProofUrl.imageBack = this.idproofs.IDProofUrl.imageBack;

    //  this.register.aadharProofUrl.imageFront = this.aadharproofs.aadharProofUrl.imageFront;
    //  this.register.aadharProofUrl.imageBack = this.aadharproofs.aadharProofUrl.imageBack;

    if (this.aadharProofList.length == 2) {
      this.images = true;
    }
    // if(this.register.aadharProofUrl.imageFront){
    //   this.aadharfront = true;
    // }
    // if(this.register.aadharProofUrl.imageBack){
    //   this.aadharbackside = true;
    // }

  }

  UserReg = false;
  UserLogin = true;
  userReg() {
    this.Aadhar = false;
    this.IdProofs = false;
    this.UserReg = true;
    this.UserLogin = false;
  }
  userLogin() {
    this.Aadhar = false;
    this.IdProofs = false;
    this.UserReg = false;
    this.UserLogin = true;
  }

  user = {
    userName: "",
    password: ""
  }

  register = {
    "firstName": "",
    "lastName": "",
    "gender": "",
    "userName": "",
    "email": "",
    "password": "",
    "confirmPassword": "",
    "mobileNumber": "",
    "location": "",
    "designation": "",
    "IDProofUrl": {
      "imageFront": "",
      "imageBack": ""
    },
    "aadharProofUrl": {
      "imageFront": "",
      "imageBack": ""
    }
  }

  imageURI: any;
  imageFileName: any;

  ionViewDidLoad() {
    //  console.log('ionViewDidLoad UserPage');
  }
  aadhar() {
    this.navCtrl.push("UploadimagePage", { data: 'Aadhar' })
  }
  deptId() {
    this.navCtrl.push("UploadimagePage", { data: 'DeptId' })
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  aadharProofList = [];
  IdproofsList = [];
  AadharStaus = false;
  IdproofsStaus = false;


  aadharProof() {

    this.AadharStaus = true;
    this.IdproofsStaus = false;
    this.Aadhar = true;
    this.IdProofs = false;
    this.UserReg = true;
    this.UserLogin = false;
  }


  Idproofs() {
    this.AadharStaus = false;
    this.IdproofsStaus = true;
    // this.aadharProofList = [];
    // this.IdproofsList = []
    this.transscreen = true;
    this.Aadhar = false;
    this.IdProofs = true;
    this.UserReg = true;
    this.UserLogin = false;
  }
  imgLoaded() {
    // if (this.register.aadharProofUrl.imageFront == "") {
    //   this.presentToast("please upload image properly");
    //   console.log("this.register.aadharProofUrl.imageFront", this.register.aadharProofUrl.imageFront)
    // }

    //  if (this.register.aadharProofUrl.imageBack == "") {
    //   let alert = this.alert.create({

    //     subTitle: 'Please Upload Images properly',
    //     buttons: ['Dismiss']
    //   });
    //   alert.present();
    // }


    if (this.register.aadharProofUrl.imageFront && this.register.aadharProofUrl.imageBack) {
      console.log("two on submit", this.register.aadharProofUrl.imageFront, this.register.aadharProofUrl.imageBack)
      this.Aadhar = false;
      this.IdProofs = false;
      this.UserReg = true;
      this.UserLogin = false;
    }
    else {
      let alert = this.alert.create({

        subTitle: 'Please Upload Images properly',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }
  imgLoaded1() {
    if (this.register.IDProofUrl.imageFront && this.register.IDProofUrl.imageBack) {
      this.Aadhar = false;
      this.IdProofs = false;
      this.UserReg = true;
      this.UserLogin = false;
    }
    else {
      let alert = this.alert.create({
        subTitle: 'Please Upload Images properly',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }
  actionSheet() {

    let actionSheet = this.actionsheet.create({
      title: 'Update a Picture using',
      buttons: [
        {
          text: 'Camera',
          role: 'Camera',
          icon: 'camera',
          handler: () => {
            this.takePhoto()
          }
        }, {
          text: 'Gallery',
          role: 'Gallery',
          icon: 'images',
          handler: () => {
            this.choosePhoto()
          }
        }, {
          icon: 'close',
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  sourceType: number;
  choosePhoto() {
    console.info("image");

    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0,
    }
    console.log(options);
    this.camera.getPicture(options).then((imageData) => {
      console.log('Image Selected');
      let base64Image = 'data:image/jpeg;base64,' + imageData;

      if (this.AadharStaus) {
        if (this.aadharProofList.length < 2) {
          this.aadharProofList.push(base64Image);
          console.log("Two images by choose", this.aadharProofList)

          //  if (this.aadharProofList[0] != "") {
          //     this.register.aadharProofUrl.imageBack = this.aadharProofList[1];
          //     console.log("back_take",  this.register.aadharProofUrl.imageBack)
          //   }
          //   else {
          //     this.register.aadharProofUrl.imageFront = this.aadharProofList[0];
          //     console.log("fornt_take",  this.register.aadharProofUrl.imageFront)
          //   }

          this.register.aadharProofUrl.imageFront = this.aadharProofList[0];
          this.register.aadharProofUrl.imageBack = this.aadharProofList[1];
        }
      }
      if (this.IdproofsStaus) {
        if (this.IdproofsList.length < 2) {
          this.IdproofsList.push(base64Image)
          // if (this.IdproofsList[0] != "") {
          //   this.register.IDProofUrl.imageBack = this.IdproofsList[1];
          //   console.log("back_take",  this.register.aadharProofUrl.imageBack)
          // }
          // else {
          //   this.register.IDProofUrl.imageFront = this.IdproofsList[0];
          //   console.log("fornt_take",  this.register.aadharProofUrl.imageFront)
          // }
          this.register.IDProofUrl.imageFront = this.IdproofsList[0];
          this.register.IDProofUrl.imageBack = this.IdproofsList[1];
        }
      }
    }, (err) => {

      console.log(err);
    });
  }

  takePhoto() {
    const options: CameraOptions = {

      quality: 100,
      targetWidth: 120,
      targetHeight: 120,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }
    this.camera.getPicture(options, ).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      if (this.AadharStaus) {
        if (this.aadharProofList.length < 2) {
          this.aadharProofList.push(base64Image)
          this.register.aadharProofUrl.imageFront = this.aadharProofList[0];
          this.register.aadharProofUrl.imageBack = this.aadharProofList[1];

        }
      }
      else if (this.IdproofsStaus) {
        if (this.IdproofsList.length < 2) {
          this.IdproofsList.push(base64Image)
          this.register.IDProofUrl.imageFront = this.IdproofsList[0];
          this.register.IDProofUrl.imageBack = this.IdproofsList[1];

        }
      }

    },
      (err) => {
        console.log(err);
      });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      if (imageData.code == 200) {
        this.imageURI = imageData;
      }
      console.info('imageUrl', this.imageURI);
    }, (err) => {
      console.log(err);
      // this.presentToast(err);
    });
  }
  //imageFileName:any;
  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: 'image.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    console.log(this.imageURI);
    fileTransfer.upload(this.imageURI,
      'http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/file/filetoupload',
      options)
      .then((data: any) => {
        console.info('filename', JSON.parse(JSON.stringify(data.response)));
        let x = data.response;
        console.log(JSON.parse(x).data);
        this.register.IDProofUrl = JSON.parse(x).data;
        console.log(JSON.stringify(this.register));
        this.Register();
        loader.dismiss();
        // this.presentToast("Image uploaded successfully");
      }, (err) => {
        // console.log(JSON.stringify(err));
        loader.dismiss();
        this.presentToast(err);
      });
  }

  uploadImage() {
    console.info("photoDetails", this.myphoto);

    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: "filetoupload" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.myphoto, 'http://192.168.1.30/api/upload/uploadFoto.php', options)
      .then((data) => {
        alert("Success");
        //loader.dismiss();
      }, (err) => {
        console.log(err);
        alert("Error");
        //loader.dismiss();
      });
  }

  Login() {
    var url = "http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/userLogin"
    this.serviceProvider.Logindetails(url, this.user)
      .subscribe(data => {
        // console.log(data);
        if (data.success) {
          this.navCtrl.setRoot('DashboardPage');
        }
        else {
          let alert = this.alert.create({
            title: 'Login Failed!',
            subTitle: data.msg,
            buttons: ['OK']
          });
          alert.present();
        }
      })
  }

  Register() {

    var url = "http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/userRegister";
    console.log("RegisterCalled:", this.register);
    this.serviceProvider.CreateRegister(url, this.register)
      .subscribe(data => {

        console.log("text");
        console.log(data);
        //   console.log(data.data);
        if (data.success) {
          let loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: 'Please Wait'
          });
          loading.present();
          let alert = this.alert.create({
            title: 'Successfully registered',
            buttons: [
              {
                text: 'ok',
                handler: () => {
                  this.UserLogin = true;
                  this.UserReg = false;
                }
              }]
          });
          loading.dismiss();
          alert.present();
          // this.navCtrl.setRoot('LoginPage');
        }
        else {
          let alert = this.alert.create({
            title: 'Enter your details',
            buttons: ['OK']
          });
          alert.present();
        }
      },
        err => {
          console.info("Err", JSON.stringify(err));
        })
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';


@IonicPage()
@Component({
  selector: 'page-uploadimage',
  templateUrl: 'uploadimage.html',
})
export class UploadimagePage {

  previousData: any;
  constructor(public navCtrl: NavController, private camera: Camera,
    public actionsheet: ActionSheetController, public navParams: NavParams) {
    this.previousData = this.navParams.get('data')
  }

  IDProofUrl = {
    imageFront: "",
    imageBack: ""
  }
  aadharProofUrl = {
    imageFront: "",
    imageBack: ""
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadimagePage');
  }
  actionSheet() {
    let actionSheet = this.actionsheet.create({

      title: 'Update a Picture using',
      buttons: [
        {

          text: 'Camera',
          role: 'Camera',
          icon: 'camera',
          handler: () => {
            this.takePhoto()

          }
        }, {

          text: 'Gallery',
          role: 'Gallery',
          icon: 'images',
          handler: () => {
            this.choosePhoto()
          }
        }, {
          icon: 'close',
          text: 'Cancel',
          role: 'cancel',

          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  sourceType: number;
  choosePhoto() {
    console.info("image");
    // const options: CameraOptions = {
    //   allowEdit: true,
    //   quality: 100,
    //   targetWidth: 150,
    //   targetHeight: 150,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.PNG,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   correctOrientation: true,
    // }
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0,
    }
    console.log(options);
    this.camera.getPicture(options).then((imageData) => {
      console.log('Image Selected');
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      // console.log(base64Image);
      this.IDProofUrl.imageFront = base64Image;

      this.IDProofUrl.imageBack = base64Image;
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }
  takePhoto() {
    const options: CameraOptions = {
      // allowEdit: true,
      quality: 100,
      targetWidth: 120,
      targetHeight: 120,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }
    this.camera.getPicture(options, ).then((imageData) => {
      let base64Image = "data:image/png;base64," + imageData;
      this.aadharProofUrl.imageFront = base64Image;

      this.aadharProofUrl.imageBack = base64Image;

    },
      (err) => {
        console.log(err);
      });
  }
  imageLoaded() {
    this.navCtrl.setRoot("UserPage", {register:'true',login:'false', 'idProofImages': this.IDProofUrl, 'aadharProofImages': this.aadharProofUrl})
   
  }
  imgLoaded() {
    this.navCtrl.setRoot("UserPage", {register:'true',login:'false', 'idProofImages': this.IDProofUrl, 'aadharProofImages': this.aadharProofUrl})
   
  }
}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';


import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-darshan-booking',
  templateUrl: 'darshan-booking.html',
})
export class DarshanBookingPage implements OnInit{

  ngOnInit(): void {
    this.payload= {
      // "referredBy": "",
      "employeeName": this.svc.userDetails.userName,
          //   "bookedFor": "",
      "employeeEmail": this.svc.userDetails.email,
      "employeeMobileNumber": this.svc.userDetails.mobileNumber,
      "referenceType": "Self",//new
      "employeeDesignation": this.svc.userDetails.designation,
      "employeePlaceOfWork": this.svc.userDetails.location,
      "darshandate": "",
      "darshanType": "",//new
      "guestsinfo": [],//new
      "guestName": "",
      "guestMobileNumber": "",
      "guestEmail": "",
      
    };
  }
  public isVisible: boolean = false;
  public footerisVisible: boolean = true;
  myphoto: any;
  bookRequest: string;
  public backgroundImage = 'assets/imgs/background/img1.png';
  QuantityIncrease: any[];
  guestTable: boolean = false;
  noOfGuests;
  guestName = [];
  age = [];
  gender = [];
  IDProofType = [];
  IDProof = [];
  bookingInfo: boolean = true;
  confirmbtn: boolean = false;
  userInfo: boolean;
  imageURI: any;
  imageFileName: any;
  payload :any;
  constructor(public navCtrl: NavController,
    public svc: ServiceProvider, public navParams: NavParams,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public alert: AlertController) {
      this.resetGuestInformation();
   
  }
  guestsInformation = {
    "guestName": '',
    "age": '',
    "gender": '',
    "IDProofType": 'AAdhar',
    "IDProof": 'AAdhar',
    "IDProofUrl": 'https://goo.gl/images/u9JPoL',
    
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad DarshanPage');
    
  }

  selectGuestInfoo() {
    this.bookingInfo = false;
    this.userInfo = true;
    this.footerisVisible=false;
  }
  i = 1;
  selectGuestInfo() {

    if (this.i < 7) {

      this.payload.guestsinfo[this.i - 1] = this.guestsInformation;

      this.i++;
      this.guestsInformation = {
        "guestName": '',
        "age": '',
        "gender": '',
        "IDProofType": '',
        "IDProof": '',
        "IDProofUrl": '',
        
      };

      console.log("guestno", this.QuantityIncrease)

      let alert = this.alert.create({
        title: 'Successfully Added',
        buttons: [
          {
            text: 'ok',
            role: 'cancel',
            handler: () => {
              this.bookingInfo = true;
              this.userInfo = false;
              this.confirmbtn = true;
              this.guestTable = true;
              this.bookRequest = "success"
            }
          }
        ]

      });
      alert.present();
    }
    else {

      let alert = this.alert.create({
        title: 'only allowed are six members',
        buttons: ['OK'],

      });
      alert.present();

    }
    this.bookingInfo = false;
    this.userInfo = true;
  }
  userDetails() {

    console.log(this.payload)
    this.bookingInfo = true;
    this.userInfo = false;
    this.confirmbtn = true;
    this.guestTable = true;
    console.log("getinfo", this.payload.guestsinfo)
  }


  // file upload
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  takePhoto(sourceType: number) {
    console.info("image");
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0,
    }
    console.log(options);
    this.camera.getPicture(options).then((imageData) => {
      console.log('Image Selected');
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      // console.log(base64Image);
      this.guestsInformation.IDProofUrl = base64Image;
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      console.info('imageUrl', this.imageURI);
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }


  addGuestDetails() {

    console.log(this.payload.guestsinfo.length);
    if (this.payload.guestsinfo.length <= 6) {
      this.payload.guestsinfo.push(this.guestsInformation);
      this.resetGuestInformation();
      console.info('payload', this, this.payload);
    }
    else{
      this.presentToast("You are allowed you add 6 piligrms only.");
      }
      this.isVisible =true;
      this.bookingInfo=true;
      this.footerisVisible=true;
  }

  resetGuestInformation() {
    this.payload== {
     
      "employeeName": "",
        
      "employeeEmail": "",
      "employeeMobileNumber": "",
      "referenceType": "",
      "employeeDesignation": "",
      "employeePlaceOfWork": "",
      "darshandate": "",
      "darshanType": "",
      "guestsinfo": [],
      "guestName": "",
      "guestMobileNumber": "",
      "guestEmail": "",
  
    };
    this.guestsInformation = {
      "guestName": '',
      "age": '',
      "gender": '',
      "IDProofType": 'AAdhar',
      "IDProof": 'AAdhar',
      "IDProofUrl": ''
      
    };
  }
  goBack(){
    this.bookingInfo=true;
  }

  removeGuestDetails(index) {
    this.payload.guestsinfo.splice(index, 1);
  }



  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: 'image.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    console.log(this.imageURI);
    fileTransfer.upload(this.imageURI,
      'http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/file/filetoupload',
      options)
      .then((data: any) => {
        console.info('filename', JSON.parse(JSON.stringify(data.response)));
        let x = data.response;
        console.log(JSON.parse(x).data);
        // this.payload.guestsinfo[0].IDProofUrl = JSON.parse(x).data;
        this.guestsInformation.IDProofUrl = JSON.parse(x).data;
        console.log(JSON.stringify(this.payload));
        this.selectGuestInfo();
        // loader.dismiss();
        // this.presentToast("Image uploaded successfully");
      }, (err) => {
        // console.log(JSON.stringify(err));
        //loader.dismiss();
        this.presentToast(err);
      });
  }



  uploadImage() {
    console.info("photoDetails", this.myphoto);

    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: "filetoupload" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.myphoto, 'http://192.168.1.30/api/upload/uploadFoto.php', options)
      .then((data) => {
        alert("Success");
        //loader.dismiss();
      }, (err) => {
        console.log(err);
        alert("Error");
        //loader.dismiss();
      });
  }

  // file upload
  booking() {
    var url = "http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/bookDarshan";
   // console.log("bookingrooms",JSON.stringify(this.payload));
    this.svc.BookingRooms(url, this.payload)
      .subscribe(data => {
        JSON.stringify(data);
        console.log("kkkkkkkkkkkkkkkkk",data)
        if (data.success == true) {
          let alert = this.alert.create({
            title: 'Thank you!',
            subTitle: 'Your reservation is now confirmed.',
            buttons: [
              {
                text: 'ok',
                role: 'cancel',
                handler: () => {
                   this.navCtrl.push('DashboardPage');
                }
              }
            ]

          });
          alert.present();
        }
        if (data.success == false) {
          let alert = this.alert.create({

            subTitle: 'please select all fields',
            buttons: [
              {
                text: 'ok',
                role: 'cancel',
                handler: () => {
                  // this.navCtrl.push('');
                }
              }
            ]

          });
          alert.present();
        }

        console.log(data);
     //   console.log(this.payload)
      })
  }

}

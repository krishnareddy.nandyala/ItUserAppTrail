import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DarshanBookingPage } from './darshan-booking';

@NgModule({
  declarations: [
    DarshanBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(DarshanBookingPage),
  ],
})
export class DarshanBookingPageModule {}

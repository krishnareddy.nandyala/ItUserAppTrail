import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';



@IonicPage()
@Component({
  selector: 'page-room-booking',
  templateUrl: 'room-booking.html',
})
export class RoomBookingPage implements OnInit {

  designation: any;
  imageURI: any;
  checkRadioButton = "false"
  confirmbtn: boolean = false;
  guestTable: boolean = false;
  isDarshan: boolean = false;
  isDarshanNo: boolean = true;
  Guestinfo: boolean = true;
  Userinfo: boolean = false;
  isenabled: boolean = false;
  roomBooking: boolean = true;
  QuantityIncrease = [];
  bookingrooms: any;
  ngOnInit(): void {
    this.bookingrooms = {

      //"referredBy": "",
      "employeeName": this.svc.userDetails.userName,
      //"bookedFor": "",
      "employeeEmail": this.svc.userDetails.email,
      "employeeMobileNumber": this.svc.userDetails.mobileNumber,
      "employeeDesignation": this.svc.userDetails.designation,
      "employeePlaceOfWork": this.svc.userDetails.location,
      "optFromDate": "",
      "optToDate": "",
      "bookedBy": "PRO",
      "purpose": "",
      //"bookinginformation": Array,
      "referenceType": "Self",//new
      "isDarshanRequired": false,//new
      //new
      "darshanDate": "date",
      "darshanType": "type",//new
      "guestsinfo": [],//new
      "guestName": "",
      "guestMobileNumber": "",
      "guestEmail": "",
      //  "employeeDesignation":"hyderabad"

    };
  }
  guestsInformation = {
    "guestName": '',
    "age": '',
    "gender": '',
    // "IDProofType": '',
    // "IDProof": ''
    "IDProofType": 'AAdhar',
    "IDProof": 'AAdhar',
    "IDProofUrl": ''
  };
  guestInformation: any[];
  guestName = [];
  age = [];
  gender = [];
  IDProofType = [];
  IDProof = [];
  darshanTypes = [];
  noOfGuests;
  employeeName;
  public backgroundImage = '/assets/imgs/background/img1.png';
  constructor(public navCtrl: NavController,
    public svc: ServiceProvider, public navParams: NavParams,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public alert: AlertController, public storage: Storage) {
    this.resetGuestInformation();
  }

  Yes() {
    this.isDarshan = true;
    this.isDarshanNo = false;
    this.bookingrooms.isDarshanRequired = true;
    this.roomBooking = false;
    this.confirmbtn = false;
  }

  No() {
    this.isDarshan = false;
    this.isDarshanNo = true;
    this.confirmbtn = true;
    this.bookingrooms.isDarshanRequired = false;
  }

  selectGuestInfoo() {
    this.Guestinfo = false;
    this.Userinfo = true;
    this.roomBooking = false;
    this.guestTable = true;
  }
  i = 1;
  selectGuestInfo() {
    if (this.i < 7) {
      this.bookingrooms.guestsinfo[this.i - 1] = this.guestsInformation;
      this.i++;
      this.guestsInformation = {
        "guestName": '',
        "age": '',
        "gender": '',
        "IDProofType": 'AAdhar',
        "IDProof": 'AAdhar',
        "IDProofUrl": '',
      };

      console.log("guestno", this.QuantityIncrease)

      let alert = this.alert.create({
        title: 'Successfully Added',
        buttons: [
          {
            text: 'ok',
            role: 'cancel',
            handler: () => {
              this.Userinfo = false;
              this.confirmbtn = true;
              this.isDarshan = true;

            }
          }
        ],

      });
      alert.present();

    }
    else {

      let alert = this.alert.create({
        title: 'only allowed are six members',
        buttons: ['OK'],

      });
      alert.present();
      this.Guestinfo = true;
      this.Userinfo = false;
      this.roomBooking = true;


    }

    this.guestInformation = [this.bookingrooms.guestsinfo]
    console.log("getinformation", this.bookingrooms.guestsinfo)
    console.log("getinfo", this.guestInformation)
  }
  userDetails() {
    var tempArray = []
    console.log(this.guestsInformation)
    //   this.Guestinfo = true;
    this.Userinfo = false;
    //  this.isDarshan = true;
    this.confirmbtn = true;
  }
  // file upload

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  takePhoto(sourceType: number) {
    console.info("image");
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0,
    }
    console.log(options);
    this.camera.getPicture(options).then((imageData) => {
      console.log('Image Selected');
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      // console.log(base64Image);
      this.guestsInformation.IDProofUrl = base64Image;
    }, (err) => {
      // Handle error
      console.log(err);
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      console.info('imageUrl', this.imageURI);
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }


  addGuestDetails() {
    console.log(this.bookingrooms.guestsinfo.length);
    if (this.bookingrooms.guestsinfo.length <= 6) {
      this.bookingrooms.guestsinfo.push(this.guestsInformation);
      this.selectGuestInfo();
      //this.resetGuestInformation();
      console.info('payload', this.bookingrooms);
    }
    else {
      this.presentToast("You are allowed you add 6 piligrms only.");
    }
    this.Userinfo = false;
    this.confirmbtn = true;
    this.isDarshan = true;
  }



  // file upload

  resetGuestInformation() {
    this.bookingrooms = {


      "employeeName": "",

      "employeeEmail": "",
      "employeeMobileNumber": "",
      "employeeDesignation": "",
      "employeePlaceOfWork": "",
      "optFromDate": "",
      "optToDate": "",
      "bookedBy": "PRO",
      "purpose": "",

      "referenceType": "",
      "isDarshanRequired": false,

      "darshanDate": "",
      "darshanType": "",
      "guestsinfo": [],
      "guestName": "",
      "guestMobileNumber": "",
      "guestEmail": "",
    };
    this.guestsInformation = {
      "guestName": '',
      "age": '',
      "gender": '',
      "IDProofType": 'AAdhar',
      "IDProof": 'AAdhar',
      "IDProofUrl": ''
    };
  }
  goBack() {
    this.Userinfo = false;
    this.confirmbtn = true;
    this.isDarshan = true;
  }
  booking() {
    let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Please Wait',
      //duration: 5000
    });
    loading.present();
    var url = "http://ec2-18-188-118-33.us-east-2.compute.amazonaws.com/itbms/api/createBookingRequest"
    console.log("bookingrooms", this.bookingrooms)
    this.svc.BookingRooms(url, this.bookingrooms)
      .subscribe(data => {
        if (data.success == true) {
          loading.dismiss();
          let alert = this.alert.create({
            title: 'Thank you!',
            subTitle: 'Your reservation is now confirmed.',
            buttons: [
              {
                text: 'ok',
                role: 'cancel',
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]

          });
          alert.present();
        }

        if (data.success == false) {
          loading.dismiss();
          let alert = this.alert.create({
            subTitle: 'please select all fields',
            buttons: [
              {
                text: 'ok',
                role: 'cancel',
                handler: () => {
                  // this.navCtrl.push('');
                }
              }
            ]

          });
          alert.present();
        }
      })
  }


}

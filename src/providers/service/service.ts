import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs';
import { Storage } from '@ionic/storage';
@Injectable()
export class ServiceProvider {
  BookingRoom(arg0: any, arg1: any): any {
    throw new Error("Method not implemented.");
  }
  query: any;
  details: any;
  headers: HttpHeaders;
  
  userDetails:any;
  constructor(public http: HttpClient,private storage:Storage) {
    this.headers = new HttpHeaders({
      "Content-Type": "application/json", 
      "Accept": "application/json",
  });
  
  this.userDetails={};
    console.log('Hello RestProvider Provider');
  }
  private authorizationHeaders = new HttpHeaders();
  //get the token
  displayName = "";
  getAuthorizationHeader(): any {
    let headers = new HttpHeaders();

    var authToken;
      this.storage.get("authToken").then(x=>{
        authToken =x;
        this.authorizationHeaders = headers.set('Authorization', authToken);
    })
    return headers;
  }
  setToken(userDetails) {
    //localStorage.setItem("authToken", token);
    console.info('Adding token',userDetails.token);
    this.storage.set("authToken",userDetails.token).then(x=>{
      console.log('token Added successfully');
      this.getAuthorizationHeader();
    });
    this.storage.set("location",userDetails.location).then(x=>{
      console.log('token Added successfully');
    });
    this.storage.set('userDetails',JSON.stringify(userDetails)).then(x=>{
      console.log(x)
    })
    
  }
  
  
  Logindetails(url, data): Observable<any> {
    let userData;
    return this.http.post(url, data)
      .map(data => {
        userData=data;
        console.info("LoggedIn info:",JSON.stringify(data));
        if(userData.userDetails)
        {
          this.userDetails=userData.userDetails;
          this.setToken(userData.userDetails);
          localStorage.setItem("authToken", userData.userDetails.token);
          localStorage.setItem("location", userData.userDetails.location);
          
        }

        return data;
      });
  }

  CreateRegister(url, data): Observable<any> {
   // let headers = this.getAuthorizationHeader();
    return this.http.post(url, data)
      .map(res => {
        return res;
      });
   
  }
  BookingRooms(url, data): Observable<any> {
    // let headers = this.getAuthorizationHeader();
    // console.info('Headers',JSON.stringify(headers));
    console.info('AuthHeaders',JSON.stringify(this.authorizationHeaders));
    return this.http.post(url, data,{ headers: this.authorizationHeaders })
      .map(res => {
        return res;
      });
  }

  getDarshanTypes(url): Observable<any>{
    let headers = this.getAuthorizationHeader();
    return this.http.get(url,{ headers: this.authorizationHeaders })
    .map( res => {
      return res;
    })
  }

  createBookingRequest(url, data): Observable<any>{

    let headers = this.getAuthorizationHeader();
    return this.http.post(url, data,{ headers: this.authorizationHeaders })
    .map( res => {
      return res;
    })
  }


  postUpload(url, data): Observable<any> {
    return this.http.post(url, data)
    .map(data=>{
      return data;
    })
  }
}
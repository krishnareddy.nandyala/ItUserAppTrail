import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs';

@Injectable()
export class DarshanServiceProvider {
    private darshanPayload: Array<any>
    constructor() {

    }

    getGuestDetails(){
        return this.darshanPayload;
    }
    setGuestDetails(payLoad){
        this.darshanPayload= payLoad;
    }

}